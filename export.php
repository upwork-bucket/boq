<?php

require 'PHPExcel/PHPExcel.php';
require_once 'db.php';


$stm = $db->prepare('SELECT * FROM boq_master WHERE id = ?');
$stm->execute([$_GET['id']]);
$boqMeta = $stm->fetch(PDO::FETCH_ASSOC);

$stm = $db->prepare('SELECT * FROM boq_data WHERE master_id = ? ORDER BY id');
$stm->execute([$_GET['id']]);
$boqData = $stm->fetchAll(PDO::FETCH_ASSOC);

$total = [];
foreach ($boqData as $item) {
    $slicedArr = array_slice($item, 9, 24);
    foreach ($slicedArr as $key => $value) {
        if (!isset($total[$key])) {
            $total[$key] = 0;
        }

        $total[$key] += $value;
    }
}

$boqDataSorted = [];
$mandalCount = 1;
$gpCount = 1;
foreach ($boqData as $item) {
    if ($item['type'] == 'mandal') {
        $boqDataSorted['mandal-' . $mandalCount] = $item;
        $mandalCount++;
    } else {
        $boqDataSorted['gp-' . $gpCount] = $item;
        $gpCount++;
    }
}

$temp = trim($boqMeta['mandal_from'], ')');
$temp = explode('(', $temp);
$district = array_pop($temp);

function removeDistrict($name) {
    $temp = trim($name, ')');
    $temp = explode('(', $temp);

    return array_shift($temp);
}

$cellName = array(
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE',
    'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS',
    'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ');


$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$rowCount = 1;

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:B1');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:B2');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:B3');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:B4');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:B5');

$objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('B')->setWidth(50);
$objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('C')->setWidth(30);

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Name of District')->setCellValue('C1', $district);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', 'Type of Ring')->setCellValue('C2', $boqMeta['ring_type']);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A3', 'Originating Mandal')->setCellValue('C3', removeDistrict($boqMeta['mandal_from']));
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A4', 'Terminating Mandal')->setCellValue('C4', removeDistrict($boqMeta['mandal_to']));
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A5', 'No. of GPs in the Ring')->setCellValue('C5', $boqMeta['gp_total']);

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A6', 'S. No.')
    ->setCellValue('B6', 'Item description including Specification')
    ->setCellValue('C6', 'Unit of Measurement')
    ->setCellValue('D6', 'Quantity')
    ->setCellValue('E6', 'Mandal 1 ' . removeDistrict($boqMeta['mandal_from']));

$objPHPExcel->setActiveSheetIndex(0)->getStyle('E6:E8')->applyFromArray(array(
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'e7e6e6')
    )
));

$objPHPExcel->setActiveSheetIndex(0)->getStyle("A6:D6")->applyFromArray(array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    ),
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'a6a6a6')
    )
));

$objPHPExcel->setActiveSheetIndex(0)->getStyle("A8:A24")->applyFromArray(array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    )
));

$objPHPExcel->setActiveSheetIndex(0)->getStyle("A26:A32")->applyFromArray(array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    )
));

$objPHPExcel->setActiveSheetIndex(0)->getStyle("C8:C24")->applyFromArray(array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    )
));

$objPHPExcel->setActiveSheetIndex(0)->getStyle("C26:C32")->applyFromArray(array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    )
));

$objPHPExcel->setActiveSheetIndex(0)->getStyle("C1:C5")->applyFromArray(array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
    )
));

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E6:E7');

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->mergeCells($cellName[4 + $i] . '6:' . $cellName[4 + $i] . '7');
    $objPHPExcel->setActiveSheetIndex(0)->getStyle($cellName[4 + $i] . '6:' . $cellName[4 + $i] . '8')->applyFromArray(array(
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'e7e6e6')
        )
    ));
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '6', 'GP'. $i .' ('. $boqDataSorted['gp-' . $i]['gp_type'] .')');
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->mergeCells($cellName[4 + $gpCount] . '6:' . $cellName[4 + $gpCount] . '7');
    $objPHPExcel->setActiveSheetIndex(0)->getStyle($cellName[4 + $gpCount] . '6:' . $cellName[4 + $gpCount] . '8')->applyFromArray(array(
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'e7e6e6')
        )
    ));
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '6', 'Mandal 2 ('. removeDistrict($boqMeta['mandal_to']) .')');
}

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A7:D7')->setCellValue('A7', 'Supply of materials');

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A8', 'A')
    ->mergeCells('B8:D8')->setCellValue('B8', 'Optical Fiber Cable & Installation');

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '8', $boqDataSorted['gp-' . $i]['gp_mandal']);
}

$objPHPExcel->setActiveSheetIndex(0)->getStyle("A8:D8")->applyFromArray(array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'cccccc')
    )
));

$objPHPExcel->setActiveSheetIndex(0)->getStyle("A7:D7")->applyFromArray(array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '808080')
    )
));


$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A9', '1')
    ->setCellValue('B9', 'Supply of 24 Core Optical Fibre ADSS Cable as per specification- wind speed 100 Km/hour with span length up to 100 meters as per TEC/GR/TX/OFC- 022/02/MAR-17 (Type IIIA)')
    ->setCellValue('C9', 'kms')
    ->setCellValue('D9', $total['adss_supply'])
    ->setCellValue('E9', $boqDataSorted['mandal-1']['adss_supply']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '9', $boqDataSorted['gp-' . $i]['adss_supply']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '9', $boqDataSorted['mandal-2']['adss_supply']);
}

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A10', '1a')
    ->setCellValue('B10', 'Span length')
    ->setCellValue('C10', 'mtrs')
    ->setCellValue('D10', $total['span_length'])
    ->setCellValue('E10', $boqDataSorted['mandal-1']['span_length']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '10', $boqDataSorted['gp-' . $i]['span_length']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '10', $boqDataSorted['mandal-2']['span_length']);
}

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A11', '1b')
    ->setCellValue('B11', 'OFC Sag length')
    ->setCellValue('C11', 'mtrs')
    ->setCellValue('D11', $total['ofc_saq_length'])
    ->setCellValue('E11', $boqDataSorted['mandal-1']['ofc_saq_length']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '11', $boqDataSorted['gp-' . $i]['ofc_saq_length']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '11', $boqDataSorted['mandal-2']['ofc_saq_length']);
}

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A12', '1c')
    ->setCellValue('B12', 'Loop at FDMS')
    ->setCellValue('C12', 'mtrs')
    ->setCellValue('D12', $total['fdms_loop'])
    ->setCellValue('E12', $boqDataSorted['mandal-1']['fdms_loop']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '12', $boqDataSorted['gp-' . $i]['fdms_loop']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '12', $boqDataSorted['mandal-2']['fdms_loop']);
}

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A13', '1d')
    ->setCellValue('B13', 'Loop at straight joint enclosure')
    ->setCellValue('C13', 'mtrs')
    ->setCellValue('D13', $total['sje_loop'])
    ->setCellValue('E13', $boqDataSorted['mandal-1']['sje_loop']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '13', $boqDataSorted['gp-' . $i]['sje_loop']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '13', $boqDataSorted['mandal-2']['sje_loop']);
}

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A14', '1e')
    ->setCellValue('B14', 'Loop at branch joint enclosure')
    ->setCellValue('C14', 'mtrs')
    ->setCellValue('D14', $total['bje_loop'])
    ->setCellValue('E14', $boqDataSorted['mandal-1']['bje_loop']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '14', $boqDataSorted['gp-' . $i]['bje_loop']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '14', $boqDataSorted['mandal-2']['bje_loop']);
}

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A15', '1f')
    ->setCellValue('B15', 'Loop at railway crossing (UG)')
    ->setCellValue('C15', 'mtrs')
    ->setCellValue('D15', $total['rc_loop'])
    ->setCellValue('E15', $boqDataSorted['mandal-1']['rc_loop']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '15', $boqDataSorted['gp-' . $i]['rc_loop']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '15', $boqDataSorted['mandal-2']['rc_loop']);
}

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A16', '1g')
    ->setCellValue('B16', 'Loop at National Highway crossing (UG)')
    ->setCellValue('C16', 'mtrs')
    ->setCellValue('D16', $total['nhc_loop'])
    ->setCellValue('E16', $boqDataSorted['mandal-1']['nhc_loop']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '16', $boqDataSorted['gp-' . $i]['nhc_loop']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '16', $boqDataSorted['mandal-2']['nhc_loop']);
}

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A17', '1h')
    ->setCellValue('B17', 'Loop at National Highway crossing (Aerial)')
    ->setCellValue('C17', 'mtrs')
    ->setCellValue('D17', $total['nhc_loop_aerial'])
    ->setCellValue('E17', $boqDataSorted['mandal-1']['nhc_loop_aerial']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '17', $boqDataSorted['gp-' . $i]['nhc_loop_aerial']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '17', $boqDataSorted['mandal-2']['nhc_loop_aerial']);
}

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A18', '1i')
    ->setCellValue('B18', 'Loop at road crossing (Aerial)')
    ->setCellValue('C18', 'mtrs')
    ->setCellValue('D18', $total['rc_loop_aerial'])
    ->setCellValue('E18', $boqDataSorted['mandal-1']['rc_loop_aerial']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '18', $boqDataSorted['gp-' . $i]['rc_loop_aerial']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '18', $boqDataSorted['mandal-2']['rc_loop_aerial']);
}

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A19', '1j')
    ->setCellValue('B19', '10 mtrs loop at every 500 mtrs point in span')
    ->setCellValue('C19', 'mtrs')
    ->setCellValue('D19', $total['loop_10_mtrs'])
    ->setCellValue('E19', $boqDataSorted['mandal-1']['loop_10_mtrs']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '19', $boqDataSorted['gp-' . $i]['loop_10_mtrs']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '19', $boqDataSorted['mandal-2']['loop_10_mtrs']);
}

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A20', '2')
    ->setCellValue('B20', 'Supply of Joint Enclosure, Pole Accessories')
    ->setCellValue('C20', 'kms')
    ->setCellValue('D20', $total['je_pa_supply'])
    ->setCellValue('E20', $boqDataSorted['mandal-1']['je_pa_supply']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '20', $boqDataSorted['gp-' . $i]['je_pa_supply']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '20', $boqDataSorted['mandal-2']['je_pa_supply']);
}

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A21', '3')
    ->setCellValue('B21', 'Supply of 24 Port Fiber Distribution Management System (Wall Mountable)')
    ->setCellValue('C21', 'Nos.')
    ->setCellValue('D21', $total['fiber_24_supply'])
    ->setCellValue('E21', $boqDataSorted['mandal-1']['fiber_24_supply']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '21', $boqDataSorted['gp-' . $i]['fiber_24_supply']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '21', $boqDataSorted['mandal-2']['fiber_24_supply']);
}

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A22', '4')
    ->setCellValue('B22', 'Supply of 48 Port Fiber Distribution Management System (Rack Mountable)')
    ->setCellValue('C22', 'Nos.')
    ->setCellValue('D22', $total['fiber_48_supply'])
    ->setCellValue('E22', $boqDataSorted['mandal-1']['fiber_48_supply']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '22', $boqDataSorted['gp-' . $i]['fiber_48_supply']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '22', $boqDataSorted['mandal-2']['fiber_48_supply']);
}

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A23', '5')
    ->setCellValue('B23', 'Supply of new PSC poles of 8 mts height')
    ->setCellValue('C23', 'Nos.')
    ->setCellValue('D23', $total['psc_supply'])
    ->setCellValue('E23', $boqDataSorted['mandal-1']['psc_supply']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '23', $boqDataSorted['gp-' . $i]['psc_supply']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '23', $boqDataSorted['mandal-2']['psc_supply']);
}

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A24', '6')
    ->setCellValue('B24', 'Racks (24U)')
    ->setCellValue('C24', 'Nos.')
    ->setCellValue('D24', $total['racks_supply'])
    ->setCellValue('E24', $boqDataSorted['mandal-1']['racks_supply']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '24', $boqDataSorted['gp-' . $i]['racks_supply']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '24', $boqDataSorted['mandal-2']['racks_supply']);
}


$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A25:D25')->setCellValue('A25', 'Installation and Commissioning  of materials');

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A26', 'A')
    ->mergeCells('B26:D26')->setCellValue('B26', 'Optical Fiber Cable & Installation');


$objPHPExcel->setActiveSheetIndex(0)->getStyle("A26:D26")->applyFromArray(array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'cccccc')
    )
));

$objPHPExcel->setActiveSheetIndex(0)->getStyle("A25:D25")->applyFromArray(array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '808080')
    )
));


$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A27', '1')
    ->setCellValue('B27', 'Installation of 24 Core Optical Fibre ADSS Cable as per specification- wind speed 100 Km/hour with span length up to 100 meters as per TEC/GR/TX/OFC-O22/02/MAR-17 (Type IMA)')
    ->setCellValue('C27', 'kms')
    ->setCellValue('D27', $total['adss_installation'])
    ->setCellValue('E27', $boqDataSorted['mandal-1']['adss_installation']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '27', $boqDataSorted['gp-' . $i]['adss_installation']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '27', $boqDataSorted['mandal-2']['adss_installation']);
}

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A28', '2')
    ->setCellValue('B28', 'Installation & commissioning of Joint Enclosure, Pole Accessories')
    ->setCellValue('C28', 'kms')
    ->setCellValue('D28', $total['je_pa_installation'])
    ->setCellValue('E28', $boqDataSorted['mandal-1']['je_pa_installation']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '28', $boqDataSorted['gp-' . $i]['je_pa_installation']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '28', $boqDataSorted['mandal-2']['je_pa_installation']);
}


$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A29', '3')
    ->setCellValue('B29', 'Installation & commissioning of 24 Port Fiber Distribution Management System (Wall Mountable)')
    ->setCellValue('C29', 'Nos.')
    ->setCellValue('D29', $total['fiber_24_installation'])
    ->setCellValue('E29', $boqDataSorted['mandal-1']['fiber_24_installation']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '29', $boqDataSorted['gp-' . $i]['fiber_24_installation']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '29', $boqDataSorted['mandal-2']['fiber_24_installation']);
}

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A30', '4')
    ->setCellValue('B30', 'Installation & commissioning of 48 Port Fiber Distribution Management System (Rack Mountable')
    ->setCellValue('C30', 'Nos.')
    ->setCellValue('D30', $total['fiber_48_installation'])
    ->setCellValue('E30', $boqDataSorted['mandal-1']['fiber_48_installation']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '30', $boqDataSorted['gp-' . $i]['fiber_48_installation']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '30', $boqDataSorted['mandal-2']['fiber_48_installation']);
}

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A31', '5')
    ->setCellValue('B31', 'Installation & commissioning of new PSC poles of 8 mts height')
    ->setCellValue('C31', 'Nos.')
    ->setCellValue('D31', $total['psc_installation'])
    ->setCellValue('E31', $boqDataSorted['mandal-1']['psc_installation']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '31', $boqDataSorted['gp-' . $i]['psc_installation']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '31', $boqDataSorted['mandal-2']['psc_installation']);
}

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A32', '6')
    ->setCellValue('B32', 'Racks (24U)')
    ->setCellValue('C32', 'Nos.')
    ->setCellValue('D32', $total['racks_installation'])
    ->setCellValue('E32', $boqDataSorted['mandal-1']['racks_installation']);

for ($i = 1; $i < $gpCount; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $i] . '32', $boqDataSorted['gp-' . $i]['racks_installation']);
}

if (isset($boqDataSorted['mandal-2'])) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[4 + $gpCount] . '32', $boqDataSorted['mandal-2']['racks_installation']);
}


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="export.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
// Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
// always modified
header('Cache-Control: cache, must-revalidate');
// HTTP/1.1
header('Pragma: public');
// HTTP/1.0
$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');