<?php
error_reporting(0);
error_reporting(E_ALL);
require_once '../session.php';
require_once '../db.php';

$boqMeta = [];
$boqData = [];

if (isset($_GET['mode']) && isset($_GET['id']) && $_GET['mode'] == 'edit') {
    $stm = $db->prepare('SELECT * FROM boq_master WHERE id = ?');
    $stm->execute([$_GET['id']]);
    $boqMeta = $stm->fetch(PDO::FETCH_ASSOC);

    unset($boqMeta['id']);
    unset($boqMeta['status']);
    unset($boqMeta['remarks']);

    if (!$boqMeta) {
        print json_encode([
            'success' => false,
            'response' => 'Data not found'
        ]);
        die();
    }

    if ($_SESSION['type'] != $boqMeta['pia']) {
        print json_encode([
            'success' => false,
            'response' => 'Permission denied'
        ]);
        die();
    }

    $stm = $db->prepare('SELECT * FROM boq_data WHERE master_id = ?');
    $stm->execute([$_GET['id']]);
    $result = $stm->fetchAll(PDO::FETCH_ASSOC);

    $boqData = [];
    $mandalCount = 1;
    $gpCount = 1;
    foreach ($result as &$item) {
        $type = $item['type'];

        unset($item['master_id']);
        unset($item['type']);

        if ($type == 'mandal') {
            $boqData['mandal-' . $mandalCount] = $item;
            $mandalCount++;
        } else {
            $boqData['gp-' . $gpCount] = $item;
            $gpCount++;
        }
    }
}

$stm = $db->prepare('SELECT DISTINCT district, mandal, mandal_code FROM lcd_codes ORDER BY mandal');
$stm->execute();
$mandals = $stm->fetchAll(PDO::FETCH_ASSOC);

$stm = $db->prepare('SELECT DISTINCT mandal, mandal_code FROM lcd_codes ORDER BY mandal_code');
$stm->execute();
$result = $stm->fetchAll(PDO::FETCH_ASSOC);
$mandalCodes = [];

foreach ($result as $item) {

    if (!isset($mandalCodes[$item['mandal_code']])) {

        $mandalCodes[$item['mandal_code']] = [];
    }
    $mandalCodes[$item['mandal_code']][] = $item['mandal'];
}

$stm = $db->prepare('SELECT DISTINCT mandal, mandal_code, lgc FROM lcd_codes ORDER BY lgc');
$stm->execute();
$result = $stm->fetchAll(PDO::FETCH_ASSOC);
$gpCodes = [];

foreach ($result as $item) {
    if (!isset($gpCodes[$item['mandal_code']])) {
        $gpCodes[$item['mandal_code']] = [];
    }

    $gpCodes[$item['mandal_code']][] = $item['lgc'];
}

$stm = $db->prepare('SELECT DISTINCT ring_id FROM boq_master');
$stm->execute();
$rings = $stm->fetchAll(PDO::FETCH_ASSOC);

$ringIDs = [];
foreach ($rings as $item) {
    $idParts = explode('_', $item['ring_id']);

    if (!isset($ringIDs[$idParts[0] . '_' . $idParts[1]])) {
        $ringIDs[$idParts[0] . '_' . $idParts[1]] = [];
    }

    $ringIDs[$idParts[0] . '_' . $idParts[1]][] = intval($idParts[2]);
}

print json_encode([
    'success' => true,
    'meta' => $boqMeta,
    'data' => $boqData,
    'mandals' => $mandals,
    'ringIDs' => $ringIDs,
    'mandalCodes' => $mandalCodes,
    'gpCodes' => $gpCodes
]);
