<?php
require_once '../session.php';
require_once '../db.php';

if ($_SESSION['type'] == 5 || $_SESSION['type'] == 9) {
    $stm = $db->prepare('SELECT * FROM boq_master ORDER BY id');
    $stm->execute();
    $boqMeta = $stm->fetchAll(PDO::FETCH_ASSOC);
} else {
    $stm = $db->prepare('SELECT * FROM boq_master WHERE pia = "'. $_SESSION['type'] .'" ORDER BY id');
    $stm->execute();
    $boqMeta = $stm->fetchAll(PDO::FETCH_ASSOC);
}

$submitted = 0;
$pending = 0;
$approved = 0;
$rejected = 0;

foreach ($boqMeta as $meta) {
    $submitted++;

    if ($meta['status'] == 'Pending') {
        $pending++;
    }

    if ($meta['status'] == 'Approved') {
        $approved++;
    }

    if ($meta['status'] == 'Rejected') {
        $rejected++;
    }
}

print json_encode([
    'user' => $_SESSION['type'],
    'boq' => $boqMeta,
    'submitted' => $submitted,
    'pending' => $pending,
    'approved' =>$approved,
    'rejected' => $rejected
]);