<?php

session_start();

require_once '../db.php';

if ($_SESSION['type'] != 6 && $_SESSION['type'] != 7 && $_SESSION['type'] != 8) {
    die('Permission denied!');
}

$stmt = $db->prepare('DELETE FROM boq_master WHERE id = ?');
$stmt->execute([$_POST['id']]);

$stmt = $db->prepare('DELETE FROM boq_data WHERE master_id = ?');
$stmt->execute([$_POST['id']]);