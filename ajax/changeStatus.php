<?php

session_start();

require_once '../db.php';

if ($_SESSION['type'] != 5) {
    die('Permission denied!');
}

$stmt = $db->prepare('UPDATE boq_master SET status = ?, remarks = ? WHERE id = ?');
$stmt->execute([$_POST['status'], $_POST['remarks'], $_POST['id']]);