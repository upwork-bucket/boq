<?php

session_start();

require_once '../db.php';

if ($_SESSION['type'] != 6 && $_SESSION['type'] != 7 && $_SESSION['type'] != 8) {
    print json_encode([
        'success' => false,
        'msg' => 'Permission denied!'
    ]);
    die();
}

if (isset($_POST['action']) && $_POST['action'] == 'add') {
    $stmt = $db->prepare('INSERT INTO boq_master (
                                ring_type,
                                mandal_from,
                                mandal_to,
                                mandal_from_code,
                                mandal_to_code,
                                ring_id,
                                gp_number_ring,
                                gp_number_spur,
                                gp_total,
                                fiber_length,
                                pia,
                                status,
                                remarks
                            )
                            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
    $stmt->execute([
        $_POST['meta']['ring_type'],
        $_POST['meta']['mandal_from'],
        $_POST['meta']['mandal_to'],
        $_POST['meta']['mandal_from_code'],
        $_POST['meta']['mandal_to_code'],
        $_POST['meta']['ring_id'],
        $_POST['meta']['gp_number_ring'],
        $_POST['meta']['gp_number_spur'],
        $_POST['meta']['gp_total'],
        $_POST['meta']['fiber_length'],
        $_SESSION['type'],
        'Pending',
        ''
    ]);

    $masterID = $db->lastInsertId();

    foreach ($_POST['data'] as $key => $data) {
        $type = explode('-', $key);

        $columnString = 'master_id,' . implode(',', array_keys($data)) . ',type';
        $valueString = implode(',', array_fill(0, count($data) + 2, '?'));

        $stmt = $db->prepare("INSERT INTO boq_data ({$columnString}) VALUES ({$valueString})");

        if ($type[0] == 'mandal') {
            $stmt->execute(array_merge([$masterID], array_values($data), ['mandal']));
        } else {
            $stmt->execute(array_merge([$masterID], array_values($data), ['gp']));
        }
    }

    print json_encode([
        'success' => true,
        'msg' => 'OK'
    ]);
    die();
}

if (isset($_POST['action']) && $_POST['action'] == 'edit') {
    foreach ($_POST['data'] as $data) {
        $id = $data['id'];

        $updateString = '';
        $values = [];
        foreach ($data as $name => $value) {
            $updateString .= $name . ' = ?,';
            $values[] = $value;
        }

        $updateString = trim($updateString, ',');
        $values[] = $id;

        $stmt = $db->prepare("UPDATE boq_data SET ". $updateString ." WHERE id = ?");
        $stmt->execute($values);
    }

    print json_encode([
        'success' => true,
        'msg' => 'OK'
    ]);
    die();
}