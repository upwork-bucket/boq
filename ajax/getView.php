<?php
require_once '../session.php';
require_once '../db.php';

$stm = $db->prepare('SELECT * FROM boq_master WHERE id = ?');
$stm->execute([$_GET['id']]);
$boqMeta = $stm->fetch(PDO::FETCH_ASSOC);

$stm = $db->prepare('SELECT * FROM boq_data WHERE master_id = ? ORDER BY id');
$stm->execute([$_GET['id']]);
$boqData = $stm->fetchAll(PDO::FETCH_ASSOC);

$total = [];
foreach ($boqData as $item) {
    $slicedArr = array_slice($item, 9, 24);
    foreach ($slicedArr as $key => $value) {
        if (!isset($total[$key])) {
            $total[$key] = 0;
        }

        $total[$key] += $value;
    }
}

$boqDataSorted = [];
$mandalCount = 1;
$gpCount = 1;
foreach ($boqData as $item) {
    if ($item['type'] == 'mandal') {
        $boqDataSorted['mandal-' . $mandalCount] = $item;
        $mandalCount++;
    } else {
        $boqDataSorted['gp-' . $gpCount] = $item;
        $gpCount++;
    }
}

$temp = trim($boqMeta['mandal_from'], ')');
$temp = explode('(', $temp);
$district = array_pop($temp);

$table = '<table class="report-header mt-3">
        <tbody>
        <tr>
            <td>Name of District</td>
            <td>'. $district. '</td>
        </tr>
        <tr>
            <td>Type of Ring</td>
            <td>'. $boqMeta['ring_type'] .'</td>
        </tr>
        <tr>
            <td>Originating Mandal</td>
            <td>'. removeDistrict($boqMeta['mandal_from']) .'</td>
        </tr>
        <tr>
            <td>Terminating Mandal</td>
            <td>'. removeDistrict($boqMeta['mandal_to']) .'</td>
        </tr>
        <tr>
            <td>No. of GPs in the Ring</td>
            <td>'. $boqMeta['gp_total'] .'</td>
        </tr>
        </tbody>
    </table>';


// Row 1
$table .= '<table class="main-report mt-3">
            <tr>
                <td class="centered table-header">S. No.</td>
                <td class="centered table-header">Item description including Specification</td>
                <td class="centered table-header">Unit of Measurement</td>
                <td class="centered table-header">Quantity</td>
                <td class="centered light-gray-bg" rowspan="2">Mandal 1 <br>('. removeDistrict($boqMeta['mandal_from']) .')</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td class="centered light-gray-bg" rowspan="2">GP'. $i .' <br>('. $boqDataSorted['gp-' . $i]['gp_type'] .')</td>';
}

if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td class="centered light-gray-bg" rowspan="2">Mandal 2 <br>('. removeDistrict($boqMeta['mandal_to']) .')</td>';
}

$table .= '</tr>';

// Row 2
$table .= '<tr>
                <td class="centered font-weight-bold dark-bg" colspan="4">Supply of materials</td>
            </tr>';

// Row 3
$table .= '<tr>
                <td class="centered light-gray-bg font-weight-bold">A</td>
                <td class="centered light-gray-bg font-weight-bold" colspan="3">Optical Fiber Cable & Installation</td>
                <td class="centered light-gray-bg"></td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td class="centered light-gray-bg">'. $boqDataSorted['gp-' . $i]['gp_mandal'] .'</td>';
}

$table .= '<td class="centered light-gray-bg"></td></tr>';

// Row 4
$table .= '<tr>
                <td>1</td>
                <td>Supply of 24 Core Optical Fibre ADSS Cable as per specification- wind speed 100 Km/hour with span length up to 100 meters as per TEC/GR/TX/OFC- 022/02/MAR-17 (Type IIIA)</td>
                <td>kms</td>
                <td>'. $total['adss_supply'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['adss_supply'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['adss_supply'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['adss_supply'] .'</td>';
}
$table .= '</tr>';

// Row 5
$table .= '<tr>
                <td>1a</td>
                <td>Span length</td>
                <td>mtrs</td>
                <td>'. $total['span_length'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['span_length'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['span_length'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['span_length'] .'</td>';
}
$table .= '</tr>';

// Row 6
$table .= '<tr>
                <td>1b</td>
                <td>OFC Sag length</td>
                <td>mtrs</td>
                <td>'. $total['ofc_saq_length'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['ofc_saq_length'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['ofc_saq_length'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['ofc_saq_length'] .'</td>';
}
$table .= '</tr>';

// Row 7
$table .= '<tr>
                <td>1c</td>
                <td>Loop at FDMS</td>
                <td>mtrs</td>
                <td>'. $total['fdms_loop'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['fdms_loop'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['fdms_loop'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['fdms_loop'] .'</td>';
}
$table .= '</tr>';

// Row 8
$table .= '<tr>
                <td>1d</td>
                <td>Loop at straight joint enclosure</td>
                <td>mtrs</td>
                <td>'. $total['sje_loop'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['sje_loop'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['sje_loop'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['sje_loop'] .'</td>';
}
$table .= '</tr>';

// Row 9
$table .= '<tr>
                <td>1e</td>
                <td>Loop at branch joint enclosure</td>
                <td>mtrs</td>
                <td>'. $total['bje_loop'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['bje_loop'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['bje_loop'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['bje_loop'] .'</td>';
}
$table .= '</tr>';

// Row 10
$table .= '<tr>
                <td>1f</td>
                <td>Loop at railway crossing (UG)</td>
                <td>mtrs</td>
                <td>'. $total['rc_loop'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['rc_loop'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['rc_loop'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['rc_loop'] .'</td>';
}
$table .= '</tr>';

// Row 11
$table .= '<tr>
                <td>1g</td>
                <td>Loop at National Highway crossing (UG)</td>
                <td>mtrs</td>
                <td>'. $total['nhc_loop'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['nhc_loop'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['nhc_loop'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['nhc_loop'] .'</td>';
}
$table .= '</tr>';

// Row 12
$table .= '<tr>
                <td>1h</td>
                <td>Loop at National Highway crossing (Aerial)</td>
                <td>mtrs</td>
                <td>'. $total['nhc_loop_aerial'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['nhc_loop_aerial'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['nhc_loop_aerial'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['nhc_loop_aerial'] .'</td>';
}
$table .= '</tr>';

// Row 13
$table .= '<tr>
                <td>1i</td>
                <td>Loop at road crossing (Aerial)</td>
                <td>mtrs</td>
                <td>'. $total['rc_loop_aerial'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['rc_loop_aerial'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['rc_loop_aerial'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['rc_loop_aerial'] .'</td>';
}
$table .= '</tr>';

// Row 14
$table .= '<tr>
                <td>1j</td>
                <td>10 mtrs loop at every 500 mtrs point in span</td>
                <td>mtrs</td>
                <td>'. $total['loop_10_mtrs'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['loop_10_mtrs'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['loop_10_mtrs'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['loop_10_mtrs'] .'</td>';
}
$table .= '</tr>';

// Row 15
$table .= '<tr>
                <td>2</td>
                <td>Supply of Joint Enclosure, Pole Accessories</td>
                <td>kms</td>
                <td>'. $total['je_pa_supply'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['je_pa_supply'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['je_pa_supply'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['je_pa_supply'] .'</td>';
}
$table .= '</tr>';

// Row 16
$table .= '<tr>
                <td>3</td>
                <td>Supply of 24 Port Fiber Distribution Management System (Wall Mountable)</td>
                <td>Nos.</td>
                <td>'. $total['fiber_24_supply'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['fiber_24_supply'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['fiber_24_supply'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['fiber_24_supply'] .'</td>';
}
$table .= '</tr>';

// Row 17
$table .= '<tr>
                <td>4</td>
                <td>Supply of 48 Port Fiber Distribution Management System (Rack Mountable)</td>
                <td>Nos.</td>
                <td>'. $total['fiber_48_supply'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['fiber_48_supply'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['fiber_48_supply'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['fiber_48_supply'] .'</td>';
}
$table .= '</tr>';

// Row 18
$table .= '<tr>
                <td>5</td>
                <td>Supply of new PSC poles of 8 mts height</td>
                <td>Nos.</td>
                <td>'. $total['psc_supply'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['psc_supply'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['psc_supply'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['psc_supply'] .'</td>';
}
$table .= '</tr>';

// Row 19
$table .= '<tr>
                <td>6</td>
                <td>Racks (24U)</td>
                <td>Nos.</td>
                <td>'. $total['racks_supply'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['racks_supply'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['racks_supply'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['racks_supply'] .'</td>';
}
$table .= '</tr>';

// Row 20
$table .= '<tr>
                <td class="centered font-weight-bold dark-bg" colspan="4">Installation and Commissioning  of materials</td>
                <td class="centered light-gray-bg"></td>';
for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td class="centered light-gray-bg"></td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td class="centered light-gray-bg"></td>';
}
$table .= '</tr>';

// Row 21
$table .= '<tr>
                <td class="centered light-gray-bg font-weight-bold">A</td>
                <td class="centered light-gray-bg font-weight-bold" colspan="3">Optical Fiber Cable & Installation</td>';
for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td></td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td></td>';
}
$table .= '</tr>';

// Row 22
$table .= '<tr>
                <td>1</td>
                <td>Installation of 24 Core Optical Fibre ADSS Cable as per specification- wind speed 100 Km/hour with span length up to 100 meters as per TEC/GR/TX/OFC-O22/02/MAR-17 (Type IMA)</td>
                <td>kms</td>
                <td>'. $total['adss_installation'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['adss_installation'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['adss_installation'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['adss_installation'] .'</td>';
}
$table .= '</tr>';

// Row 23
$table .= '<tr>
                <td>2</td>
                <td>Installation & commissioning of Joint Enclosure, Pole Accessories</td>
                <td>kms</td>
                <td>'. $total['je_pa_installation'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['je_pa_installation'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['je_pa_installation'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['je_pa_installation'] .'</td>';
}
$table .= '</tr>';

// Row 24
$table .= '<tr>
                <td>3</td>
                <td>Installation & commissioning of 24 Port Fiber Distribution Management System (Wall Mountable)</td>
                <td>Nos.</td>
                <td>'. $total['fiber_24_installation'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['fiber_24_installation'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['fiber_24_installation'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['fiber_24_installation'] .'</td>';
}
$table .= '</tr>';

// Row 25
$table .= '<tr>
                <td>4</td>
                <td>Installation & commissioning of 48 Port Fiber Distribution Management System (Rack Mountable)</td>
                <td>Nos.</td>
                <td>'. $total['fiber_48_installation'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['fiber_48_installation'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['fiber_48_installation'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['fiber_48_installation'] .'</td>';
}
$table .= '</tr>';

// Row 26
$table .= '<tr>
                <td>5</td>
                <td>Installation & commissioning of new PSC poles of 8 mts height</td>
                <td>Nos.</td>
                <td>'. $total['psc_installation'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['psc_installation'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['psc_installation'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['psc_installation'] .'</td>';
}
$table .= '</tr>';

// Row 27
$table .= '<tr>
                <td>6</td>
                <td>Racks (24U)</td>
                <td>Nos.</td>
                <td>'. $total['racks_installation'] .'</td>
                <td>'. $boqDataSorted['mandal-1']['racks_installation'] .'</td>';

for ($i = 1; $i < $gpCount; $i++) {
    $table .= '<td>'. $boqDataSorted['gp-' . $i]['racks_installation'] .'</td>';
}
if (isset($boqDataSorted['mandal-2'])) {
    $table .= '<td>'. $boqDataSorted['mandal-2']['racks_installation'] .'</td>';
}
$table .= '</tr>';

$table .= '</table>';

function removeDistrict($name) {
    $temp = trim($name, ')');
    $temp = explode('(', $temp);

    return array_shift($temp);
}

print $table;